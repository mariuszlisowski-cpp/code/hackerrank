// Hackerrank
// 30 days of code
// Day 18: Queues and Stacks

#include <iostream>

using namespace std;

// ------------------------------ solution ------------------------------
class StackElement {
public:
  char data;
  StackElement* under = NULL;
  StackElement(char ch) {
    data = ch;
    under = NULL;
  }
};

class QueueElement {
public:
  char data;
  QueueElement* after = NULL;
  // constructor
  QueueElement(char ch) {
    data = ch;
    after = NULL;
  }
};

class Solution {
public:
  QueueElement* first = NULL;
  QueueElement* last = NULL;
  StackElement* top = NULL;
  // stack methods
  void pushCharacter(char);
  char popCharacter();
  // queue methods
  void enqueueCharacter(char);
  char dequeueCharacter();
};

void Solution::pushCharacter(char ch) {
  StackElement* newElement = new StackElement(ch);
  newElement->under = top;
  top = newElement;
}

char Solution::popCharacter() {
  if (top == NULL)
    return '\0';
  StackElement* temp = top;
  char ch = top->data;
  top = top->under;
  delete temp;
  return ch;
}

void Solution::enqueueCharacter(char ch) {
    QueueElement* newElement = new QueueElement(ch);
    if (first == NULL)
      first = last = newElement;
    else {
      last->after = newElement;
      last = newElement;
    }
}

char Solution::dequeueCharacter() {
  char ch;
  if (first != NULL) {
    QueueElement* temp = first;
    ch = temp->data;
    first = temp->after;
    delete temp;
  }
  return ch;
}
// ----------------------------------------------------------------------

int main() {
    // read the string s.
    string s;
    getline(cin, s);

    // create the Solution class object p.
    Solution obj;

    // push/enqueue all the characters of string s to stack.
    for (int i = 0; i < s.length(); i++) {
        obj.pushCharacter(s[i]);
        obj.enqueueCharacter(s[i]);
    }

    bool isPalindrome = true;

    // pop the top character from stack.
    // dequeue the first character from queue.
    // compare both the characters.
    for (int i = 0; i < s.length() / 2; i++) {
        if (obj.popCharacter() != obj.dequeueCharacter()) {
            isPalindrome = false;
            break;
        }
    }

    // finally print whether string s is palindrome or not.
    if (isPalindrome) {
        cout << "The word, " << s << ", is a palindrome.";
    } else {
        cout << "The word, " << s << ", is not a palindrome.";
    }

    return 0;
}
