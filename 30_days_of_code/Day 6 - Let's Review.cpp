// Hackerrank
// 30 days of code
// Day 6: Let's Review

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int main() {
    int t;
    string s, odd;

    cin >> t;
    while (t--) {
        odd = "";
        cin >> s;
        for (int i = 0; i < s.size(); i++) {
        if (i % 2 == 0) {
            cout << s[i];
        }
        else
            odd += s[i];
        }
        cout << " " << odd << endl;
    }
    return 0;
}
