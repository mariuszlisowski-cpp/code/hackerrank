// Hackerrank
// 30 days of code
// Day 25: Running Time and Complexity

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

bool isPrime (int);

int main() {
   int n, t;
   cin >> t;
   while (t--) {
      cin >> n;
      if (isPrime(n))
         cout << "Prime" << endl;
      else
         cout << "Not prime" << endl;
   }
   return 0;
}

bool isPrime(int number) {
   if (number < 2) return false;
   if (number == 2) return true;
   if (number % 2 == 0) return false;
   for (int i = 3; (i*i) <= number; i += 2)
      if (number % i == 0 ) return false;
   return true;
}
