// Hackerrank
// 30 days of code
// Day 26: Nested Logic

#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>

using namespace std;

int main() {
   int day, month, year, eday, emonth, eyear, fine;

   cin >> day >> month >> year;
   cin >> eday >> emonth >> eyear;

   if (year > eyear)
      fine = 10000;
   else if (year == eyear && month > emonth)
      fine = 500 * (month - emonth);
   else if (year == eyear && month == emonth && day > eday)
      fine = 15 * (day - eday);
   else fine = 0;

    cout << fine;

    return 0;
}
