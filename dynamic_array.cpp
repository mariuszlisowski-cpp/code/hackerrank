// dynamic array
// hackerrank.com

#include <iostream>
#include <vector>

using namespace std;

vector<int> dynamicArray(int n, vector<vector<int>> queries) {
    int lastAnswer {};
    vector<int> output;
    vector<vector<int>> seqList(n);
    size_t size {queries.size()};
    for (size_t i = 0; i < size; ++i) {
        if (queries[i][0] == 1) {
            auto x = queries[i][1];
            auto seq = (x ^ lastAnswer) % n;
            seqList[seq].push_back(queries[i][2]);
        }
        if (queries[i][0] == 2) {
            auto x = queries[i][1];
            auto seq = (x ^ lastAnswer) % n;
            lastAnswer = seqList[seq][queries[i][2] % seqList[seq].size()];
            cout << lastAnswer << endl;
            output.push_back(lastAnswer);
        }
    }
    return output;
}

int main() {
    vector<vector<int>> input {
        {1, 0, 5},
        {1, 1, 7},
        {1, 0, 3},
        {2, 1, 0},
        {2, 1, 1}
    };

    vector<int> output = dynamicArray(2, input);

    return 0;
}
