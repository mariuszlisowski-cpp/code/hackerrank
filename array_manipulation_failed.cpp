// array manipulation
// FAILED: processing time exceeded
// hackerrank.com

#include <algorithm>
#include <array>
#include <iostream>
#include <list>
#include <vector>

using namespace std;

template <class T>
void displayList(const T& ls) {
    for (const auto& e : ls)
        cout << e << " ";
    cout << endl;
}

long arrayManipulation(int n, vector<vector<int>> queries) {
    // list<int> l(n);
    vector<long> v(n);

    // verbose
    displayList(v);

    size_t size = queries.size();
    for (size_t i = 0; i < size; ++i) {
        int a = queries[i].at(0);
        int b = queries[i].at(1);
        int k = queries[i].at(2);

        auto beg = next(v.begin(), a - 1);
        auto end = next(v.begin(), b);
        for_each(beg, end, [&k](long& i) { i += k; });

        // or..
        // for_each(v.begin() + queries[i][0] - 1, v.begin() + queries[i][1], [&i, &queries](long& e) { e += queries[i][2]; });

        // verbose
        displayList(v);
    }

    // verbose
    cout << "Max: ";

    return *max_element(v.begin(), v.end());
}

int main() {
    vector<vector<int>> vv{
        { 1, 5, 3 },
        { 4, 8, 7 },
        { 6, 9, 1 }
    };

    cout << arrayManipulation(10, vv);

    return 0;
}