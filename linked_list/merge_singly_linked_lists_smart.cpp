// merge two singly linked lists

#include <memory>
#include <iostream>

class SinglyLinkedListNode;

using namespace std;
using LinkedListPtr = std::shared_ptr<SinglyLinkedListNode>;

class SinglyLinkedListNode {
public:
    int data{};
    LinkedListPtr next;
    
    SinglyLinkedListNode() {}
    SinglyLinkedListNode(int node_data) {
        data = node_data;
        next = nullptr;
    }
};

LinkedListPtr insertNodeAtTail(LinkedListPtr head, int data) {
    LinkedListPtr node = std::make_shared<SinglyLinkedListNode>(data);
    if (!head) {
        head = node;
    }
    else {
        LinkedListPtr last = head;
        while (last->next)
            last = last->next;
        last->next = node;
    }

    return head;
}

LinkedListPtr mergeLists(LinkedListPtr head1, LinkedListPtr head2) {
    if (!head1 && !head2) {
        return nullptr;
    }
    if (!head1) {
        return head2;
    }
    if (!head2) {
        return head1;
    }
    LinkedListPtr head;
    LinkedListPtr last;
    if (head1->data < head2->data) {
        head = head1;
        head1 = head1->next;
    } else {
        head = head2;
        head2 = head2->next;
    }
    last = head;

    while (head1 && head2) {
        if (head1->data < head2->data) {
            last->next = head1;
            last = head1;
            head1 = head1->next;
        } else {
            last->next = head2;
            last = head2;
            head2 = head2->next;
        }
    }
    if (head1) {
        last->next = head1;
    }
    if (head2) {
        last->next = head2;
    }

    return head;
}

LinkedListPtr mergeListsFake(LinkedListPtr head1, LinkedListPtr head2) {
    if (!head1 && !head2) {
        return nullptr;
    }
    LinkedListPtr fake = std::make_shared<SinglyLinkedListNode>();
    LinkedListPtr last = fake;

    while (head1 && head2) {
        if (head1->data < head2->data) {
            last->next = head1;
            last = head1;
            head1 = head1->next;
        } else {
            last->next = head2;
            last = head2;
            head2 = head2->next;
        }
    }
    if (head1) {
        last->next = head1;
    }
    if (head2) {
        last->next = head2;
    }

    return fake->next;
}

void displayNodes(LinkedListPtr head) {
    while (head) {
        cout << head->data << " -> ";
        head = head->next;
    }
    cout << "nullptr" << endl;
}

int main() {
    LinkedListPtr head1;
    // sorted
    head1 = insertNodeAtTail(head1, 1);
    head1 = insertNodeAtTail(head1, 5);
    head1 = insertNodeAtTail(head1, 7);
    displayNodes(head1);

    LinkedListPtr head2;
    // sorted
    head2 = insertNodeAtTail(head2, 3);
    head2 = insertNodeAtTail(head2, 4);
    head2 = insertNodeAtTail(head2, 5);
    displayNodes(head2);

    auto head = mergeListsFake(head1, head2);
    displayNodes(head);

    return 0;
}
