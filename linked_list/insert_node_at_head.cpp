// insert node at head

#include <iostream>

using namespace std;

class SinglyLinkedListNode {
public:
    int data;
    SinglyLinkedListNode* next;
    SinglyLinkedListNode(int node_data) {
        this->data = node_data;
        this->next = nullptr;
    }
};

SinglyLinkedListNode* insertNodeAtHead(SinglyLinkedListNode* head, int data) {
    SinglyLinkedListNode* node = new SinglyLinkedListNode(data);
    if (!head)
        head = node;
    else {
        node->next = head;
        head = node;
    }
    return head;
}

void displayNodes(SinglyLinkedListNode* head) {
    while (head) {
        cout << head->data << " -> ";
        head = head->next;
    }
    cout << "nullptr" << endl;
}

int main() {
    SinglyLinkedListNode* head{};

    head = insertNodeAtHead(head, 10);
    head = insertNodeAtHead(head, 20);
    head = insertNodeAtHead(head, 30);
    displayNodes(head);

    return 0;
}
