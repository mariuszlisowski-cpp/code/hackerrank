// insert data into singly linked list

#include <iostream>
#include <iterator>
#include <vector>

using namespace std;

class SinglyLinkedListNode {
public:
    int data_;
    SinglyLinkedListNode* next;

    SinglyLinkedListNode(int data)
        : data_(data), next(nullptr) {}
};

////////////////////////////////////////////////////////////////////////////////
// SOLUTION
SinglyLinkedListNode* sortedInsert(SinglyLinkedListNode* head, int data) {
    SinglyLinkedListNode* node = new SinglyLinkedListNode(data);
    if (!head) {
        head = node;
    } else {
        SinglyLinkedListNode* curr = head;
        SinglyLinkedListNode* trail{};
        // traverse
        while (curr) {
            if (data > curr->data_) {
                trail = curr;
                curr = curr->next;
            } else {
                break;
            }
        }
        // insert at head
        if (curr == head) {
            node->next = head; // join node at right
            head = node;
        } 
        // insert at the end & between
        else {
            node->next = curr;   // join node at right (to node or nullptr)
            trail->next = node;  // join node at left
        }
    }

    return head;
}
////////////////////////////////////////////////////////////////////////////////

void displayList(SinglyLinkedListNode* head) {
    while (head) {
        cout << head->data_ << " -> ";
        head = head->next;
    }
    cout << "nullptr" << endl;
}

int main() {
    SinglyLinkedListNode* head{};

    head = sortedInsert(head, 30);
    head = sortedInsert(head, 10);
    head = sortedInsert(head, 10);
    head = sortedInsert(head, 20);
    head = sortedInsert(head, 40);

    displayList(head);

    return 0;
}
