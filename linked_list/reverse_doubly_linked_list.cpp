// reverse doubly linked list

#include <iostream>

class DoublyLinkedListNode {
public:
    DoublyLinkedListNode(int data):data_(data) {}

    int data_{};
    DoublyLinkedListNode* next{};
    DoublyLinkedListNode* prev{};
};

DoublyLinkedListNode* reverse(DoublyLinkedListNode* head) {
    DoublyLinkedListNode* temp;
    while (head) {
        temp = head->prev;
        head->prev = head->next;
        head->next = temp;
        if (head->prev) {
            head = head->prev;
        } else {
            break;
        }
    }
    return head;
}

DoublyLinkedListNode* insertNodeAtTail(DoublyLinkedListNode* head, int data) {
    DoublyLinkedListNode* node = new DoublyLinkedListNode(data);
    if (!head) {
        head = node;
    } else {
        DoublyLinkedListNode* last = head;
        while (last->next) {
            last = last->next;
        }
        last->next = node;
        node->prev = last;
    }
    return head;
}

void displayDoublyLinkedList(DoublyLinkedListNode* head) {
    while (head) {
        std::cout << head->data_ << ' ';
        head = head->next;
    }
    std::cout << '\n';
}

int main() {
    DoublyLinkedListNode* head{};

    head = insertNodeAtTail(head, 30);
    head = insertNodeAtTail(head, 20);
    head = insertNodeAtTail(head, 10);

    displayDoublyLinkedList(head);
    head = reverse(head);
    displayDoublyLinkedList(head);

    head = insertNodeAtTail(head, 40);
    head = insertNodeAtTail(head, 50);
    head = insertNodeAtTail(head, 60);

    displayDoublyLinkedList(head);
    head = reverse(head);
    displayDoublyLinkedList(head);

    return 0;
}