// insert data into doubly linked list

#include <iostream>

class DoublyLinkedListNode {
public:
    DoublyLinkedListNode(int data):data_(data) {}

    int data_{};
    DoublyLinkedListNode* next{};
    DoublyLinkedListNode* prev{};
};

////////////////////////////////////////////////////////////////////////////////
// SOLUTION
DoublyLinkedListNode* sortedInsert(DoublyLinkedListNode* head, int data) {
    DoublyLinkedListNode* node = new DoublyLinkedListNode(data);
    if (!head) {
        return node;
    }
    DoublyLinkedListNode* curr = head;
    DoublyLinkedListNode* trail{};
    // traverse
    while (curr) {
        if (data > curr->data_) {
            trail = curr;
            curr = curr->next;
        } else {
            break;
        }
    }
    // insert at head
    if (curr == head) {
        head->prev = node; // join node at right
        node->next = head; // join node at right
        head = node;
    } 
    // insert at the end & between
    else {
        trail->next = node; // join node at left
        node->prev = trail; // join node at left
        // insert between
        if (curr) {
            curr->prev = node; // join node at right
            node->next = curr; // join node at right
        }
    }

    return head;
}
////////////////////////////////////////////////////////////////////////////////

void displayDoublyLinkedList(DoublyLinkedListNode* head) {
    while (head) {
        std::cout << head->data_ << ' ';
        head = head->next;
    }
    std::cout << '\n';
}

int main() {
    DoublyLinkedListNode* head{};

    head = sortedInsert(head, 10);
    head = sortedInsert(head, 30);
    head = sortedInsert(head, 10);
    head = sortedInsert(head, 20);
 
    displayDoublyLinkedList(head);

    return 0;
}