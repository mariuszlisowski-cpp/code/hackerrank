// display nodes from tail

#include <iostream>
#include <vector>

using namespace std;

class SinglyLinkedListNode {
public:
    int data;
    SinglyLinkedListNode* next;
    SinglyLinkedListNode(int node_data) {
        this->data = node_data;
        this->next = nullptr;
    }
};

SinglyLinkedListNode* insertNodeAtTail(SinglyLinkedListNode* head, int data) {
    SinglyLinkedListNode* node = new SinglyLinkedListNode(data);
    if (!head)
        head = node;
    else {
        SinglyLinkedListNode* last = head;
        while (last->next)
            last = last->next;
        last->next = node;
    }
    return head;
}

void displayNodes(SinglyLinkedListNode* head) {
    while (head) {
        cout << head->data << " -> ";
        head = head->next;
    }
    cout << "nullptr" << endl;
}

void displayNodesReverse(SinglyLinkedListNode* head) {
    // base case to exit recursion (nullptr)
    if (!head)
        return;
    if (!head->next)
        cout << "nullptr";  // condition can be omitted
    displayNodesReverse(head->next);
    // returning form recursion
    cout << " <- " << head->data;
}

int main() {
    SinglyLinkedListNode* head{};

    head = insertNodeAtTail(head, 10);
    head = insertNodeAtTail(head, 20);
    head = insertNodeAtTail(head, 30);
    displayNodes(head);
    displayNodesReverse(head);

    return 0;
}
