// compare two singly linked lists

#include <iostream>

using namespace std;

class SinglyLinkedListNode {
public:
    int data;
    SinglyLinkedListNode* next;
    SinglyLinkedListNode(int node_data) {
        this->data = node_data;
        this->next = nullptr;
    }
};

SinglyLinkedListNode* insertNodeAtTail(SinglyLinkedListNode* head, int data) {
    SinglyLinkedListNode* node = new SinglyLinkedListNode(data);

    if (!head)
        head = node;
    else {
        SinglyLinkedListNode* last = head;
        while (last->next)
            last = last->next;
        last->next = node;
    }

    return head;
}

bool compareLists(SinglyLinkedListNode* head1, SinglyLinkedListNode* head2) {
    // check data in nodes
    while (head1 && head2) {
        // different data
        if (head1->data != head2->data)
            return false;
        head1 = head1->next;
        head2 = head2->next;
    }
    // diffrent sizes
    if ((head1 && !head2) || (!head1 && head2))
        return false;

    // same data & same sizes
    return true;
}

void displayNodes(SinglyLinkedListNode* head) {
    while (head) {
        cout << head->data << " -> ";
        head = head->next;
    }
    cout << "nullptr" << endl;
}

int main() {
    SinglyLinkedListNode* head1{};

    head1 = insertNodeAtTail(head1, 10);
    head1 = insertNodeAtTail(head1, 20);
    head1 = insertNodeAtTail(head1, 30);
    displayNodes(head1);

    SinglyLinkedListNode* head2{};

    head2 = insertNodeAtTail(head2, 10);
    head2 = insertNodeAtTail(head2, 20);
    head2 = insertNodeAtTail(head2, 90);
    displayNodes(head2);

    if (compareLists(head1, head2))
        cout << "Lists are the same!" << endl;
    else
        cout << "Lists are NOT the same!" << endl;

    return 0;
}
