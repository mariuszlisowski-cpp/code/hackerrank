// insert data into singly linked list

#include <iostream>
#include <iterator>
#include <vector>

using namespace std;

class SinglyLinkedListNode {
public:
    int data_;
    SinglyLinkedListNode* next;
    
    SinglyLinkedListNode(int data)
        : data_ (data), next(nullptr)
    {}
};

////////////////////////////////////////////////////////////////////////////////
// SOLUTION
SinglyLinkedListNode* sortedInsert(SinglyLinkedListNode* head, int data) {
    SinglyLinkedListNode* node = new SinglyLinkedListNode(data);
    // insert the first node
    if (!head) {
        return node;
    }
    // insert at the beginning
    if (data <= head->data_) {
        node->next = head;
        return node;
    }
    // insert within
    SinglyLinkedListNode* curr = head;
    SinglyLinkedListNode* trail{};
    while (curr) {
        if (data > curr->data_) {
            trail = curr;
            curr = curr->next;
        } else {
            node->next = curr;
            trail->next = node;
            break;
        }
    }
    // insert at the end
    trail->next = node;

    return head;
}
////////////////////////////////////////////////////////////////////////////////

void displayList(SinglyLinkedListNode* head) {
    while (head) {
        cout << head->data_ << " -> ";
        head = head->next;
    }
    cout << "nullptr" << endl;
}

int main() {
    SinglyLinkedListNode* head{};

    head = sortedInsert(head, 30);
    head = sortedInsert(head, 10);
    head = sortedInsert(head, 10);
    head = sortedInsert(head, 20);
    head = sortedInsert(head, 40);

    displayList(head);


    return 0;
}
