#include <algorithm>
#include <iostream>
#include <string>

int camelcase_words_count(const std::string& s) {
    return std::count_if(std::begin(s), std::end(s), 
                  [](unsigned char c) {
                      return std::isupper(c);
                  }) + 1;

}

int main() {
    std::string str{"oneTwoThreeFourFive"};

    std::cout << "Words count: " << camelcase_words_count(str) << std::endl;

    return 0;
}
