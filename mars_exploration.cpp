#include <iostream>
#include <string>

// SOS SPS SQS SOR  // 3
//      x   x    x

int marsExploration(std::string s) {
    constexpr size_t signal_length = 3;
    size_t signals_number = s.size() / signal_length;
    std::string correct_sos{"SOS"};
    size_t counter{};
    for (size_t i = 0; i < s.size(); i += signal_length) {
        std::string sub_sos = s.substr(i, 3);
        if (sub_sos.size() == signal_length) {
            for (size_t i = 0; i < signal_length; ++i) {
                if (sub_sos[i] != correct_sos[i]) {
                    ++counter;
                }
            }
        }
    }

    return counter;
}

int main() {
    std::string help{"SOSSPSSQSSOR"};

    std::cout << marsExploration(help) << std::endl;


    return 0;
}
