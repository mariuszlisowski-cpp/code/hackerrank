#include <iostream>
#include <algorithm>

void print_array(int* arr, int size) {
    for (int i{}; i < size; ++i) {
        std::cout << arr[i] << ' ';
    }
}

int main() {
    int arr[]{4, 3, 2, 0, 1, 5, 6};                                         // odd number of elements

    auto size = sizeof(arr) / sizeof(*arr);

    std::sort(arr, arr + size);

    print_array(arr, size);

    auto median = arr[size / 2];
    std::cout << std::endl << median;

    return 0;
}
