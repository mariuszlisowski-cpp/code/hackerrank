/* Military (24-hour) time converter to AM/PM format. */
#include <algorithm>
#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <string>

std::string time_conversion_from_military(std::string s) {
    std::unordered_set<std::string> am_hours{
        "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11"
    };
    std::unordered_map<std::string, std::string> pm_hours{
        {"12", "12"},
        {"13", "01"},
        {"14", "02"},
        {"15", "03"},
        {"16", "04"},
        {"17", "05"},
        {"18", "06"},
        {"19", "07"},
        {"20", "08"},
        {"21", "09"},
        {"22", "10"},
        {"23", "11"},
    };

    std::string hour;
    auto colon{s.find(":")};
    if (colon != std::string::npos) {
        hour = s.substr(0, colon);
    }

    auto is_valid_am_hour = am_hours.find(hour) != am_hours.end();
    auto is_valid_pm_hour = pm_hours.find(hour) != pm_hours.end();

    if (is_valid_am_hour) {
        s += "AM";
    } else if (is_valid_pm_hour) {
        s.replace(0, colon, pm_hours.find(hour)->second);
        s += "PM";
    } else {
        s.replace(0, colon, "12");
        s += "AM";
    }

    return s;
}

int main() {
    std::string input{"00:40:22"}; // 12:40:22AM
    // std::string input{"19:05:45"}; // 07:05:45PM
    // std::string input{"07:05:45"}; // 07:05:45AM (no change)
    // std::string input{"12:45:54"}; // 12:45:54PM (no change)
    
    std::cout << time_conversion_from_military(input);

    return 0;
}
