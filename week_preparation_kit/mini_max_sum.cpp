#include <algorithm>
#include <iostream>
#include <numeric>
#include <vector>

void miniMaxSum(std::vector<int> arr) {
    std::sort(arr.begin(), arr.end());
    arr.erase(std::remove(arr.begin(), arr.end(), 0), arr.end());

    uint64_t sum{std::accumulate(arr.begin(), arr.end(), 0llu)};
    uint64_t minSum{sum - *(arr.end() - 1)};
    uint64_t maxSum{sum - *arr.begin()};

    std::cout << minSum << ' ' << maxSum << std::endl;
}

int main() {
    std::vector<int> arr{0, 769082435, 210437958, 0, 673982045, 375809214, 380564127, 0};
    miniMaxSum(arr);

    return 0;
}
