#include <algorithm>
#include <iostream>
#include <vector>
#include <set>

template <typename T>
void print_array(const T& array) {
    for (const auto& el : array) {
        std::cout << el << ' ';
    }
    std::cout << std::endl;
}

auto counting_sort(const std::vector<int>& arr) {
    std::set<int> s(arr.begin(), arr.end());                                // sorted
    std::vector<int> result(arr.size(), 0);

    for (auto el : s) {
        auto occurence = std::count(arr.begin(), arr.end(), el);
        result[el] = occurence;
        // verbose
        std::cout << "occurences of " << el << ": " 
                  << occurence << std::endl;
    }

    return std::vector<int>{result.begin(), result.begin() + 100};
}

int main() {
    std::vector<int> arr{
        63, 25, 73, 1,  98, 73, 56, 84, 86, 57, 16, 83, 8,  25, 81, 56, 9,
        53, 98, 67, 99, 12, 83, 89, 80, 91, 39, 86, 76, 85, 74, 39, 25, 90,
        59, 10, 94, 32, 44, 3,  89, 30, 27, 79, 46, 96, 27, 32, 18, 21, 92,
        69, 81, 40, 40, 34, 68, 78, 24, 87, 42, 69, 23, 41, 78, 22, 6,  90,
        99, 89, 50, 30, 20, 1,  43, 3,  70, 95, 33, 46, 44, 9,  69, 48, 33,
        60, 65, 16, 82, 67, 61, 32, 21, 79, 75, 75, 13, 87, 70, 33
    };

    auto result = counting_sort(arr);
    print_array(result);

    return 0;
}
